# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/remind_author_not_compliant_to_merge_mr'

RSpec.describe Triage::RemindAuthorNotCompliantToMergeMr do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      { from_gitlab_org_gitlab?: true, by_resource_author?: true }
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.merge']

  describe '#applicable?' do
    context 'when event is triggered by merge request author and is under gitlab-org/gitlab' do
      include_examples 'event is applicable'
    end

    context 'when event project is not under gitlab-org/gitlab' do
      let(:event_attrs) do
        { from_gitlab_org_gitlab?: false, by_resource_author?: true }
      end

      include_examples 'event is not applicable'
    end

    context 'when event is not triggered by merge request author' do
      let(:event_attrs) do
        { from_gitlab_org_gitlab?: true, by_resource_author?: false }
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'calls remind_author_not_compliant_to_merge_mr' do
      expect(subject).to receive(:remind_author_not_compliant_to_merge_mr)
      subject.process
    end

    it 'posts a comment' do
      body = add_automation_suffix('remind_author_not_compliant_to_merge_mr.rb') do
        <<~MARKDOWN.chomp
          Hi @#{event.event_actor_username},

          Please note that authors are not authorized to merge their own merge requests and need to seek another maintainer to merge.

          For more information please refer to:
          - [GitLab Security Compliance Controls](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html)
          - [Change Management Controls CMG-04](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/change-management.html)
          - [Feedback issue](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/63)

          /label ~"self-merge"
        MARKDOWN
      end
      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
