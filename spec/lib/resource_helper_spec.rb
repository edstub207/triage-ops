# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/resource_helper'

RSpec.describe ResourceHelper do
  let(:due_date) { Date.new(2019, 11, 17) }
  let(:release_date) { Date.new(2019, 11, 22) }

  let(:resource_klass) do
    Struct.new(:issue) do
      include ResourceHelper
    end
  end

  subject { resource_klass.new }

  around do |example|
    Timecop.freeze(2020, 0o3, 31) { example.run }
  end

  describe '#age' do
    let(:resource) { { created_at: created_at } }

    context 'with age < 1' do
      let(:created_at) { '2020-03-31' }

      it 'returns 0' do
        expect(subject.age(resource)).to eq(0)
      end
    end

    context 'with age = 1' do
      let(:created_at) { '2020-03-30' }

      it 'returns 1' do
        expect(subject.age(resource)).to eq(1)
      end
    end

    context 'with age > 1' do
      let(:created_at) { '2020-03-01' }

      it 'returns 30' do
        expect(subject.age(resource)).to eq(30)
      end
    end
  end

  describe '#closed_age' do
    let(:closed_issue) { { closed_at: closed_at } }

    context 'with age < 1' do
      let(:closed_at) { '2020-03-31' }

      it 'returns 0' do
        expect(subject.closed_age(closed_issue)).to eq(0)
      end
    end

    context 'with age = 1' do
      let(:closed_at) { '2020-03-30' }

      it 'returns 1' do
        expect(subject.closed_age(closed_issue)).to eq(1)
      end
    end

    context 'with age > 1' do
      let(:closed_at) { '2020-03-01' }

      it 'returns 30' do
        expect(subject.closed_age(closed_issue)).to eq(30)
      end
    end
  end
end
